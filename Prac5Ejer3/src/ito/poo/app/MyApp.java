package ito.poo.app;

import ito.poo.clases.Fruta;
import ito.poo.clases.Periodo;

@SuppressWarnings("unused")
public class MyApp {

	static void run() {
		Fruta f1 = new Fruta("Manzana", 6F, 10, 100);
		System.out.println(f1);
		System.out.println();
		f1.agregarPeriodo("Un mes", 4000);
		System.out.println(f1);
		System.out.println();
	}
	
	static void run2() {
		Fruta f1 = new Fruta("Manzana", 6F, 10, 100);
		System.out.println(f1);
		System.out.println();
		f1.agregarPeriodo("Febrero", 4000);
		f1.agregarPeriodo("Junio y Julio", 7500);
		System.out.println(f1);
		System.out.println();
		System.out.println(f1.devolverPeriodo(3));
		System.out.println(f1.devolverPeriodo(1));
		System.out.println();
		System.out.println(f1.costoPeriodo(f1.devolverPeriodo(3)));
		System.out.println(f1.costoPeriodo(f1.devolverPeriodo(1)));
		System.out.println(f1.gananciaEstimada(f1.devolverPeriodo(1)));
	}
	public static void main(String[] args) {
		run2();
	}
}