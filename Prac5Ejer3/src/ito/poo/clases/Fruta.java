package ito.poo.clases;

import java.util.ArrayList;

public class Fruta {

	private String nombre;
	private float extension;
	private float costoPromedio;
	private float precioVentaProm;
	private ArrayList<Periodo> periodos = new ArrayList<Periodo>();
	/*****************************/
	public Fruta() {
		super();
	}

	public Fruta(String nombre, float extension, float costoPromedio, float precioVentaProm) {
		super();
		this.nombre = nombre;
		this.extension = extension;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
	}
	/*****************************/
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getExtension() {
		return extension;
	}

	public void setExtension(float extension) {
		this.extension = extension;
	}

	public float getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public float getPrecioVentaProm() {
		return precioVentaProm;
	}

	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}

	public void setPeriodos(ArrayList<Periodo> periodos) {
		this.periodos = periodos;
	}
	/*****************************/
	public void agregarPeriodo(String tiempoCosecha, float cantCosechaxtiempo) {
		Periodo p = new Periodo (tiempoCosecha, cantCosechaxtiempo);
			this.periodos.add(p);
	}
	
	public Periodo devolverPeriodo(int i) {
		Periodo dP;
			if (i > this.periodos.size() || i < 0)
				dP = new Periodo ("null", 0);
			else 
				dP = this.periodos.get(i);
			return dP;
	}

	public boolean eliminarPeriodo(int i) {
		boolean eP;
		if (i > this.periodos.size() || i < 0)
			eP = false;
		else {
			this.periodos.remove(i);
			eP = true;
		}
		return eP;
	}
	
	public float costoPeriodo(Periodo p) {
		float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = p.getCantCosechaXTiempo() * this.costoPromedio;
			return i;
	}

	public float gananciaEstimada(Periodo p) {
		float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = (p.getCantCosechaXTiempo() * this.precioVentaProm) - costoPeriodo(p);
			return i;
	}
	/*****************************/
	@Override
	public String toString() {
		return "Fruta: " + nombre + "\nExtension: " + extension + "\nCosto Promedio: " + costoPromedio
				+ "\nPrecio Venta Promedio: " + precioVentaProm + "\nPeriodos: " + periodos;
	}
}